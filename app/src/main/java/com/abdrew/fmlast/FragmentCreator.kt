package com.abdrew.fmlast

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.abdrew.fmlast.Feature.*
import com.abdrew.fmlast.ui.albumdetails.AlbumDetailsFragment
import com.abdrew.fmlast.ui.main.MainFragment
import com.abdrew.fmlast.ui.search.SearchFragment
import com.abdrew.fmlast.ui.topalbums.TopAlbumsFragment


class FragmentCreator(private val fragmentManager: FragmentManager) {

    private fun showFragment(fragment: Fragment, bundle: Bundle) {
        fragment.arguments = bundle
        fragmentManager.beginTransaction()
            .replace(R.id.framelayout_fragment_container, fragment)
            .addToBackStack(null)
            .commit()
    }

    fun goTo(feature: Feature, bundle: Bundle) {
        when (feature) {
            MAIN -> showFragment(MainFragment(), bundle)
            ARTIST_SEARCH -> showFragment(SearchFragment(), bundle)
            TOP_ALBUMS -> showFragment(TopAlbumsFragment(), bundle)
            ALBUM_DETAILS -> showFragment(AlbumDetailsFragment(), bundle)
        }
    }

    fun goBack() {
        fragmentManager.popBackStackImmediate()
    }
}

enum class Feature {
    MAIN,
    ARTIST_SEARCH,
    TOP_ALBUMS,
    ALBUM_DETAILS,
}