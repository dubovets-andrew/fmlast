package com.abdrew.fmlast

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.abdrew.fmlast.koin.appModule
import org.koin.android.ext.koin.with
import org.koin.standalone.StandAloneContext.startKoin

class FmLastApp : Application() {
    override fun onCreate() {
        super.onCreate()

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        startKoin(listOf(appModule)).with(this)
    }
}
