package com.abdrew.fmlast.extensions

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes


fun ViewGroup.inflate(@LayoutRes layoutRes: Int) {
    LayoutInflater.from(context).inflate(layoutRes, this, true)
}