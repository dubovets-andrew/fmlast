package com.abdrew.fmlast.extensions

import android.view.View


fun View.setVisibleOrGone(isVisible: Boolean) {
    visibility = getVisibilityValue(isVisible, View.GONE)
}

fun View.setVisibleOrInvisible(isVisible: Boolean) {
    visibility = getVisibilityValue(isVisible, View.INVISIBLE)
}

private fun getVisibilityValue(isVisible: Boolean, invisibilityType: Int): Int {
    return if (isVisible) View.VISIBLE else invisibilityType
}