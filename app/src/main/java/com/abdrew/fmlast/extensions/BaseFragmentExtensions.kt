package com.abdrew.fmlast.extensions

import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat.getSystemService
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.abdrew.fmlast.R
import com.abdrew.fmlast.base.fragment.BaseFragment
import com.abdrew.fmlast.base.fragment.BasePresenter
import com.abdrew.fmlast.base.fragment.BaseFragmentView
import com.abdrew.fmlast.view.empty.EmptyView
import com.abdrew.fmlast.view.empty.EmptyViewType


fun BaseFragment<out BaseFragmentView, out BasePresenter<out BaseFragmentView>>.showEmptyView(
    frameLayout: FrameLayout,
    emptyViewType: EmptyViewType
) {
    getContext()?.let {
        val emptyView = EmptyView(it).apply { setEmptyViewType(emptyViewType) }
        replaceFrameLayoutView(frameLayout, emptyView)
    }
}

fun BaseFragment<out BaseFragmentView, out BasePresenter<out BaseFragmentView>>.replaceFrameLayoutView(
    frameLayout: FrameLayout,
    view: View
) {
    getContext()?.let { frameLayout.replace(view) }
}

fun <ViewInterface, Presenter> BaseFragment<ViewInterface, Presenter>.initToolbar(
    toolbar: Toolbar,
    initActionBar: (ActionBar) -> Unit
)
        where ViewInterface : BaseFragmentView, Presenter : BasePresenter<ViewInterface> {
    (activity as AppCompatActivity).apply {
        setSupportActionBar(toolbar)
        supportActionBar?.let { initActionBar.invoke(it) }
        invalidateOptionsMenu()
    }
}

fun <ViewInterface, Presenter> BaseFragment<ViewInterface, Presenter>.hideKeyboard()
        where ViewInterface : BaseFragmentView, Presenter : BasePresenter<ViewInterface> {
    context?.let {
        val imm: InputMethodManager? = getSystemService(it, InputMethodManager::class.java)
        imm?.hideSoftInputFromWindow(view?.windowToken, 0)
    }
}


fun <ViewInterface, Presenter> BaseFragment<ViewInterface, Presenter>.createRecyclerView(): RecyclerView?
        where ViewInterface : BaseFragmentView, Presenter : BasePresenter<ViewInterface> {
    var recyclerView: RecyclerView? = null
    context?.let { context ->
        recyclerView = RecyclerView(context).apply {
            setPadding(
                paddingLeft,
                resources.getDimensionPixelSize(R.dimen.gap_medium),
                paddingRight,
                paddingBottom
            )
            clipToPadding = false
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        }
    }
    return recyclerView
}


