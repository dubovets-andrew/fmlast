package com.abdrew.fmlast.extensions

import android.view.View
import android.widget.FrameLayout


fun FrameLayout.replace(view: View) {
    removeAllViews()
    addView(view)
}