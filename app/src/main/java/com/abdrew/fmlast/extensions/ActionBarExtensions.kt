package com.abdrew.fmlast.extensions

import androidx.appcompat.app.ActionBar
import androidx.fragment.app.Fragment
import com.abdrew.fmlast.R

fun ActionBar.initBackButton(fragment: Fragment) {
    fragment.setHasOptionsMenu(true)
    setDisplayShowHomeEnabled(true)
    setDisplayHomeAsUpEnabled(true)
    setHomeAsUpIndicator(R.drawable.ic_back)
}