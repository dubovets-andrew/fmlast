package com.abdrew.fmlast.extensions

import android.os.Build
import android.text.Html
import android.widget.TextView

fun TextView.setHtml(text: String) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        setText(Html.fromHtml(text, Html.TO_HTML_PARAGRAPH_LINES_INDIVIDUAL))
    } else {
        setText(Html.fromHtml(text))
    }
}