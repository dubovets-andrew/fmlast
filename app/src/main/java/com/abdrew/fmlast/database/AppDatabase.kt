package com.abdrew.fmlast.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.abdrew.fmlast.database.dao.AlbumDao
import com.abdrew.fmlast.database.dao.TrackDao
import com.abdrew.fmlast.database.model.AlbumEntity
import com.abdrew.fmlast.database.model.TrackEntity

const val DATABASE_NAME = "fmlast"

@Database(entities = [AlbumEntity::class, TrackEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun albumDao(): AlbumDao
    abstract fun trackDao(): TrackDao
}