package com.abdrew.fmlast.database.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    foreignKeys = [ForeignKey(
        entity = AlbumEntity::class,
        parentColumns = ["uid"],
        childColumns = ["album_id"],
        onDelete = ForeignKey.CASCADE
    )]
)
data class TrackEntity(
    @PrimaryKey var uid: Long?,
    @ColumnInfo(name = "album_id") var albumId: Long?,
    @ColumnInfo(name = "track_name") var trackName: String?,
    @ColumnInfo(name = "track_duration") var trackDuration: String?
)