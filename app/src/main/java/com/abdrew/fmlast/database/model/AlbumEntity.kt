package com.abdrew.fmlast.database.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity()
data class AlbumEntity(
    @PrimaryKey @ColumnInfo(name = "uid") var uid: Int?,
    @ColumnInfo(name = "artist_name") var artistName: String?,
    @ColumnInfo(name = "album_name") var albumName: String?,
    @ColumnInfo(name = "listeners_count") var listeners: String?,
    @ColumnInfo(name = "wiki_preview") var wikiPreview: String?,
    @ColumnInfo(name = "wiki_full") var wikiFull: String?,
    @ColumnInfo(name = "image_cover_small") var imageCoverSmall: String?,
    @ColumnInfo(name = "image_cover_large") var imageCoverLarge: String?
)