package com.abdrew.fmlast.database.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.abdrew.fmlast.database.model.AlbumEntity
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
interface AlbumDao {
    @Query("SELECT * FROM albumentity ORDER BY uid DESC")
    fun getAll(): Maybe<List<AlbumEntity>>

    @Insert
    fun insertAll(vararg users: AlbumEntity): Single<List<Long>>

    @Delete
    fun delete(user: AlbumEntity): Completable

    @Query("SELECT * FROM albumentity WHERE album_name = :albumName AND artist_name = :artistName")
    fun getAlbumByName(albumName: String, artistName: String): Maybe<AlbumEntity>
}