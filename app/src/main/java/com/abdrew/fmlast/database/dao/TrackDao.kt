package com.abdrew.fmlast.database.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.abdrew.fmlast.database.model.TrackEntity
import io.reactivex.Completable

@Dao
interface TrackDao {
    @Query("SELECT * FROM trackentity")
    fun getAll(): List<TrackEntity>

    @Insert
    fun insertAll(vararg tracks: TrackEntity): Completable

    @Delete
    fun delete(user: TrackEntity)
}