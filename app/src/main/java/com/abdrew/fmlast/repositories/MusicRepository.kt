package com.abdrew.fmlast.repositories

import com.abdrew.fmlast.api.LastFmApiService
import com.abdrew.fmlast.converter.toData
import com.abdrew.fmlast.converter.toEntity
import com.abdrew.fmlast.database.AppDatabase
import com.abdrew.fmlast.koin.IO_SCHEDULER
import com.abdrew.fmlast.koin.MAIN_SCHEDULER
import com.abdrew.fmlast.model.Album
import com.abdrew.fmlast.model.MusicItemData
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.koin.java.standalone.KoinJavaComponent.inject

class MusicRepository {
    private val database: AppDatabase by inject(AppDatabase::class.java)
    private val api: LastFmApiService by inject(LastFmApiService::class.java)

    val mainScheduler: Scheduler by inject(Scheduler::class.java, name = MAIN_SCHEDULER)
    val ioScheduler: Scheduler by inject(Scheduler::class.java, name = IO_SCHEDULER)

    public fun saveAlbum(album: String, artist: String): Completable {
        return api.searchAlbumInfo(artist, album)
            .subscribeOn(ioScheduler)
            .map { it.album.toData().toEntity() to it.album.tracks?.trackJsons }
            .flatMap { albumTracksPair ->
                database.albumDao()
                    .insertAll(albumTracksPair.first)
                    .map { it to albumTracksPair.second }
            }
            .flatMapCompletable() { idTracksPair ->
                val trackEntities = idTracksPair.second?.map { it.toEntity(idTracksPair.first[0]) }
                trackEntities?.let { database.trackDao().insertAll(*it.toTypedArray()) }
            }
    }

    public fun isAlbumSaved(album: String, artist: String): Single<Boolean> {
        return database.albumDao().getAlbumByName(album, artist).map { it -> true }
            .subscribeOn(ioScheduler)
            .switchIfEmpty(Maybe.just(false))
            .toSingle()
    }

    public fun deleteAlbum(album: String, artist: String): Completable {
        val albumDao = database.albumDao()
        return albumDao.getAlbumByName(album, artist)
            .subscribeOn(Schedulers.io())
            .flatMapCompletable { albumDao.delete(it) }
    }

    public fun getSavedAlbums(): Maybe<List<Album>> {
        return database.albumDao().getAll()
            .subscribeOn(ioScheduler)
            .flatMap { albumEntities ->
                val list = albumEntities.asSequence().map { it.toData() }.toList()
                Maybe.just(list)
            }
    }

    public fun searchArtist(query: String): Maybe<List<MusicItemData>> {
        return api.searchArtist(query)
            .map { response -> response.results.artistMatches.artists }
            .map { artists -> artists.map { artist -> MusicItemData.fromArtist(artist) } }
            .flatMapMaybe { musicItems -> Maybe.just(musicItems) }
    }

    public fun getTopAlbums(artistName: String): Maybe<List<MusicItemData>> {
        return api.searchTopAlbums(artistName)
            .subscribeOn(ioScheduler)
            .map { response -> response.topAlbums.albums }
            .map { albums -> albums.map { album -> MusicItemData.fromTopAlbum(album) } }
            .flatMapMaybe { musicItems -> Maybe.just(musicItems) }
    }
}