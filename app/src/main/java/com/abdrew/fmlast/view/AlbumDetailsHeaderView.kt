package com.abdrew.fmlast.view

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import com.abdrew.fmlast.R
import com.abdrew.fmlast.extensions.inflate
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.view_album_details_header.view.*

class AlbumDetailsHeaderView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {
    init {
        orientation = VERTICAL
        inflate(R.layout.view_album_details_header)
    }

    public fun setAlbumData(
        artistName: String,
        albumName: String,
        listenersCount: String,
        albumCoverUrl: String?
    ) {
        textview_album_name.text = albumName
        textview_artist_name.text = artistName
        textview_album_listeners.text = context.getString(R.string.listeners_count, listenersCount)

        albumCoverUrl.takeIf { !it.isNullOrEmpty() }.run {
            this?.let { path ->
                viewTreeObserver.addOnGlobalLayoutListener {
                    Picasso.get().load(path).resize(measuredWidth, measuredHeight).centerCrop()
                        .into(imageview_album_cover)
                }
            }
        }
    }
}
