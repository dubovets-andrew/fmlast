package com.abdrew.fmlast.view.checkload

import com.abdrew.fmlast.base.fragment.BaseView
import io.reactivex.Observable

interface CheckLoadView : BaseView {
    fun onClick() : Observable<Unit>
    fun setLoadingView()
    fun setCheckedView()
    fun setUncheckedView()

    var albumName: String
    var artistName: String
}