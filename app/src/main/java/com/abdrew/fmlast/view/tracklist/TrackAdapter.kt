package com.abdrew.fmlast.view.tracklist

import android.view.View
import com.abdrew.fmlast.R
import com.abdrew.fmlast.api.model.albuminfo.TrackJson
import com.abdrew.fmlast.base.list.BaseAdapter

class TrackAdapter(data: List<TrackJson>) : BaseAdapter<TrackViewHolder, TrackJson>(data) {
    override fun createHolder(view: View): TrackViewHolder {
        return TrackViewHolder(view)
    }

    override fun layoutId(): Int {
        return R.layout.view_track_item
    }

}