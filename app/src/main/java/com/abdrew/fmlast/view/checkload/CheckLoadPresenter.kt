package com.abdrew.fmlast.view.checkload

import android.util.Log
import com.abdrew.fmlast.base.fragment.BasePresenter
import com.abdrew.fmlast.repositories.MusicRepository
import com.abdrew.fmlast.view.checkload.CheckLoadButtonController.State.*
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.koin.java.standalone.KoinJavaComponent.inject

class CheckLoadPresenter(viewInterface: CheckLoadView) : BasePresenter<CheckLoadView>(viewInterface) {
    private val musicRepository: MusicRepository by inject(MusicRepository::class.java)
    private var checkButtonController: CheckLoadButtonController = createButtonController()

    override fun addViewDisposables() {
        addViewDisposable(onClick())
    }

    private fun onClick(): Disposable {
        return viewInterface.onClick()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnEach {
                when (checkButtonController.state) {
                    LOADING -> {
                    }
                    else -> viewInterface.setLoadingView()
                }
            }
            .flatMap { checkButtonController.getClickCompletable().andThen(Observable.just(it)) }
            .map { checkButtonController.state }
            .distinctUntilChanged()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                viewInterface.apply {
                    when (it) {
                        LOADING, null -> {
                        }
                        CHECKED -> setCheckedView()
                        UNCHECKED -> setUncheckedView()
                    }
                }
            }, {
                Log.d("Error", it.toString())
            })
    }

    override fun dispose() {
        checkButtonController.reset()
        super.dispose()
    }

    private fun createButtonController() = object : CheckLoadButtonController() {
        override fun check(): Completable {
            with(viewInterface) {
                return musicRepository.saveAlbum(albumName, artistName)
            }
        }

        override fun unCheck(): Completable {
            with(viewInterface) {
                return musicRepository.deleteAlbum(albumName, artistName)
            }
        }

        override fun init(): Single<Boolean> {
            with(viewInterface) {
                return musicRepository.isAlbumSaved(albumName, artistName)
            }
        }
    }


}

