package com.abdrew.fmlast.view.tracklist

import android.view.View
import android.widget.TextView
import com.abdrew.fmlast.R
import com.abdrew.fmlast.api.model.albuminfo.TrackJson
import com.abdrew.fmlast.base.list.BaseViewHolder
import java.text.SimpleDateFormat
import java.util.*

class TrackViewHolder(itemView: View) : BaseViewHolder<TrackJson>(itemView) {
    override fun bind(item: TrackJson) {
        val number: TextView = itemView.findViewById(R.id.textview_tracklist_number)
        val title: TextView = itemView.findViewById(R.id.textview_tracklist_title)
        val duration: TextView = itemView.findViewById(R.id.textview_tracklist_duration)

        val trackNumber = adapterPosition + 1
        number.text = trackNumber.toString()

        title.text = item.name

        //TODO - extract into extension
        val date = Date(item.duration.toInt() * 1000L)
        val simpleDateFormat = SimpleDateFormat("mm:ss", Locale.US)
        val durationTime = simpleDateFormat.format(date)


        duration.text = durationTime
    }

}