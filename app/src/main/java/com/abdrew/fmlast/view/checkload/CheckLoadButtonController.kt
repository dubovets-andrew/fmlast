package com.abdrew.fmlast.view.checkload

import com.abdrew.fmlast.view.checkload.CheckLoadButtonController.State.*
import io.reactivex.Completable
import io.reactivex.Single

abstract class CheckLoadButtonController {
    var state: State? = null

    abstract fun check(): Completable
    abstract fun unCheck(): Completable
    abstract fun init(): Single<Boolean>

    fun getClickCompletable(): Completable {
        return when (state) {
            LOADING -> Completable.complete()
            CHECKED -> {
                state = LOADING
                unCheck().doOnComplete { state = UNCHECKED }
            }
            UNCHECKED -> {
                state = LOADING
                check().doOnComplete { state = CHECKED }
            }
            null -> init().doOnSuccess { state = if (it) CHECKED else UNCHECKED }.ignoreElement()
        }
    }

    fun reset() {
        state = null
    }

    enum class State {
        LOADING,
        CHECKED,
        UNCHECKED
    }
}