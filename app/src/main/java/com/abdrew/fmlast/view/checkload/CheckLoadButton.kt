package com.abdrew.fmlast.view.checkload

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import com.abdrew.fmlast.R
import com.abdrew.fmlast.extensions.inflate
import com.abdrew.fmlast.extensions.replace
import com.jakewharton.rxrelay2.BehaviorRelay
import io.reactivex.BackpressureStrategy
import io.reactivex.Observable
import kotlinx.android.synthetic.main.view_check_load_button.view.*

class CheckLoadButton @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr), CheckLoadView {
    override var albumName: String = ""
    override var artistName: String = ""

    private val presenter = CheckLoadPresenter(this)
    private val onClick: BehaviorRelay<Unit> = BehaviorRelay.create()

    init {
        orientation = VERTICAL
        inflate(R.layout.view_check_load_button)
        setLoadingView()
    }

    public fun initButton(albumName: String?, artistName: String?) {
        this.albumName = albumName ?: ""
        this. artistName = artistName ?: ""

        setOnClickListener { }
        presenter.dispose()
        presenter.addViewDisposables()
        onClick.accept(Unit)
        setOnClickListener { onClick.accept(Unit) }
    }

    override fun onViewRemoved(child: View?) {
        presenter.dispose()
        super.onViewRemoved(child)
    }

    override fun onClick(): Observable<Unit> {
        return onClick.toFlowable(BackpressureStrategy.LATEST).toObservable()
    }

    override fun setLoadingView() {
        framelayout_button_content.replace(ProgressBar(context))
    }

    override fun setCheckedView() {
        setButtonDrawable(R.drawable.ic_added)
    }

    override fun setUncheckedView() {
        setButtonDrawable(R.drawable.ic_download)
    }

    private fun setButtonDrawable(@DrawableRes drawableRes: Int) {
        val imageView = ImageView(context).apply {
            setImageDrawable(ContextCompat.getDrawable(context, drawableRes))
        }
        framelayout_button_content.replace(imageView)
    }

}
