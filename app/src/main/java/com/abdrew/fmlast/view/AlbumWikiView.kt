package com.abdrew.fmlast.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import androidx.annotation.StringRes
import com.abdrew.fmlast.R
import com.abdrew.fmlast.extensions.inflate
import com.abdrew.fmlast.extensions.setHtml
import kotlinx.android.synthetic.main.view_wiki.view.*

class AlbumWikiView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {
    private var isPreview: Boolean = true

    init {
        orientation = VERTICAL
        inflate(R.layout.view_wiki)
    }

    fun setWiki(preview: String, full: String) {
        fun setWikiState(wikiState: WikiState) {
            wikiState.apply {
                textview_wiki_text.setHtml(text)
                textview_wiki_button.setText(buttonTextId)
                linearlayout_wiki_gradient.visibility = visibility
            }
        }

        val previewState = WikiState(preview, R.string.details_wiki_button_view_more, View.VISIBLE)
        setWikiState(previewState)

        textview_wiki_button.setOnClickListener {
            isPreview = !isPreview

            val wikiState = when (isPreview) {
                true -> previewState
                false -> WikiState(full, R.string.details_wiki_button_view_less, View.GONE)
            }

            setWikiState(wikiState)
        }
    }

    private data class WikiState(
        val text: String,
        @StringRes val buttonTextId: Int,
        val visibility: Int
    )
}
