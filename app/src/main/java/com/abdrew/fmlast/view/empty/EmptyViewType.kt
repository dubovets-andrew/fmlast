package com.abdrew.fmlast.view.empty

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.abdrew.fmlast.R


enum class EmptyViewType(@DrawableRes val imageRes: Int, @StringRes val textRes: Int) {
    MAIN(R.drawable.ic_audio, R.string.main_empty_view_description),
    SEARCH(R.drawable.ic_search, R.string.search_empty_view_description),
}