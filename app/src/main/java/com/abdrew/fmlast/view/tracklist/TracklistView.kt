package com.abdrew.fmlast.view.tracklist

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.abdrew.fmlast.R
import com.abdrew.fmlast.api.model.albuminfo.TrackJson
import com.abdrew.fmlast.extensions.inflate
import kotlinx.android.synthetic.main.view_tracklist.view.*

class TracklistView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {
    init {
        orientation = VERTICAL
        inflate(R.layout.view_tracklist)
    }

    public fun setTrackList(trackJsons: List<TrackJson>) {
        recyclerview_tracklist.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = TrackAdapter(trackJsons)
        }
    }
}
