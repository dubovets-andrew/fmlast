package com.abdrew.fmlast.view.empty

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import com.abdrew.fmlast.R
import com.abdrew.fmlast.extensions.inflate
import kotlinx.android.synthetic.main.view_main_empty.view.*

class EmptyView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {
    init {
        orientation = VERTICAL
        inflate(R.layout.view_main_empty)
    }

    public fun setEmptyViewType(emptyViewType: EmptyViewType) {
        val drawable = ContextCompat.getDrawable(context, emptyViewType.imageRes)
        imageview_empty_icon.setImageDrawable(drawable)
        textview_empty_description.setText(emptyViewType.textRes)
    }
}
