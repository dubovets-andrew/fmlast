package com.abdrew.fmlast.koin

import androidx.room.Room
import com.abdrew.fmlast.BuildConfig
import com.abdrew.fmlast.api.LastFmApiInterceptor
import com.abdrew.fmlast.api.LastFmApiService
import com.abdrew.fmlast.database.AppDatabase
import com.abdrew.fmlast.database.DATABASE_NAME
import com.abdrew.fmlast.repositories.MusicRepository
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

const val IO_SCHEDULER: String = "io"
const val MAIN_SCHEDULER: String = "main"

val appModule = module {

    single<Retrofit> {
        Retrofit.Builder()
            .client(get() as OkHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BuildConfig.LAST_FM_API)
            .build()
    }

    single<OkHttpClient> {
        OkHttpClient.Builder()
            .addInterceptor(LastFmApiInterceptor())
            .connectTimeout(5, TimeUnit.SECONDS)
            .build()
    }

    single<LastFmApiService> { (get() as Retrofit).create(LastFmApiService::class.java) }

    single(name = MAIN_SCHEDULER) { AndroidSchedulers.mainThread() }

    single(name = IO_SCHEDULER) { Schedulers.io() }

    single {
        Room.databaseBuilder(
            androidApplication(),
            AppDatabase::class.java, DATABASE_NAME
        ).build()
    }

    single { MusicRepository() }
}