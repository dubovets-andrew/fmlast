package com.abdrew.fmlast.ui.albumdetails

import com.abdrew.fmlast.api.LastFmApiService
import com.abdrew.fmlast.base.fragment.BasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.koin.java.standalone.KoinJavaComponent.inject


class AlbumDetailsPresenter(fragmentView: AlbumDetailsView) : BasePresenter<AlbumDetailsView>(fragmentView) {
    private val lastFmApiService: LastFmApiService by inject(LastFmApiService::class.java)

    override fun addViewDisposables() {
        addViewDisposable(getAlbumInfo())
    }

    private fun getAlbumInfo(): Disposable {
        val albumName = viewInterface.getAlbumName() ?: ""
        val artistName = viewInterface.getArtistName() ?: ""

        return lastFmApiService.searchAlbumInfo(artistName, albumName)
            .doOnSubscribe { viewInterface.showLoader(true) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess { viewInterface.showLoader(false) }
            .subscribe({ it ->
                viewInterface.apply {
                    clearContentView()
                    it.album.apply { setAlbumData(artistName, albumName, listeners, images.last().path) }
                    it.album.wiki?.let { setWiki(it.summary, it.content) }

                    val tracks = it.album.tracks?.trackJsons ?: emptyList()
                    if (!tracks.isEmpty()) setTrackList(tracks)
                }
            }, { t: Throwable? -> t.toString() })
    }
}