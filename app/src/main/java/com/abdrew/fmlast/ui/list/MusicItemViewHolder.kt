package com.abdrew.fmlast.ui.list

import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.abdrew.fmlast.R
import com.abdrew.fmlast.base.list.BaseViewHolder
import com.abdrew.fmlast.model.MusicItemData
import com.squareup.picasso.Picasso

class MusicItemViewHolder(itemView: View) : BaseViewHolder<MusicItemData>(itemView) {
    override fun bind(item: MusicItemData) {
        val name: TextView = itemView.findViewById(R.id.textview_item_name)
        val listeners: TextView = itemView.findViewById(R.id.textview_item_listeners_count)
        val image: ImageView = itemView.findViewById(R.id.imageview_cover)

        name.text = item.name
        listeners.text = itemView.context.getString(R.string.listeners_count, item.listenersCount)
        val path = item.imageUrl

        path.takeIf { !it.isNullOrEmpty() }.run { Picasso.get().load(this).into(image) }
    }

    fun addActionView(view: View) {
        val actions: LinearLayout = itemView.findViewById(R.id.linearlayout_actions)
        actions.visibility = View.VISIBLE
        actions.removeAllViews()
        actions.addView(view)
    }

    override fun setOnClickListener(listener: () -> Unit) {
        val item: LinearLayout = itemView.findViewById(R.id.linearlayout_item_content)
        item.setOnClickListener { listener.invoke() }
    }
}