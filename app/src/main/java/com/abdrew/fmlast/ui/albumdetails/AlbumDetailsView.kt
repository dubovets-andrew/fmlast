package com.abdrew.fmlast.ui.albumdetails

import com.abdrew.fmlast.api.model.albuminfo.TrackJson
import com.abdrew.fmlast.base.fragment.BaseFragmentView


interface AlbumDetailsView : BaseFragmentView {
    fun getArtistName(): String?
    fun getAlbumName(): String?
    fun setTrackList(trackJsons: List<TrackJson>)
    fun setWiki(preview: String, full: String)
    fun setAlbumData(
        artistName: String,
        albumName: String,
        listenersCount: String,
        albumCoverUrl: String
    )

    fun clearContentView()
}