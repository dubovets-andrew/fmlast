package com.abdrew.fmlast.ui.search

import com.abdrew.fmlast.base.fragment.BaseFragmentView
import com.abdrew.fmlast.model.MusicItemData
import io.reactivex.Observable


interface SearchView : BaseFragmentView {
    fun onSearchButtonClicked(): Observable<String>
    fun getSearchQuery(): String
    fun showEmptyView()
    fun setArtistsList(artists: List<MusicItemData>)
    fun showArtistAlbums(artist: MusicItemData)
}