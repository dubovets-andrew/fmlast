package com.abdrew.fmlast.ui.search

import com.abdrew.fmlast.Feature
import com.abdrew.fmlast.R
import com.abdrew.fmlast.base.fragment.BaseFragment
import com.abdrew.fmlast.extensions.createRecyclerView
import com.abdrew.fmlast.extensions.hideKeyboard
import com.abdrew.fmlast.extensions.replaceFrameLayoutView
import com.abdrew.fmlast.extensions.showEmptyView
import com.abdrew.fmlast.model.MusicItemData
import com.abdrew.fmlast.ui.list.MusicItemDataAdapter
import com.abdrew.fmlast.ui.topalbums.TopAlbumsFragment
import com.abdrew.fmlast.view.empty.EmptyViewType
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_search.*


class SearchFragment : BaseFragment<SearchView, SearchPresenter>(), SearchView {
    override fun showEmptyView() {
        showEmptyView(framelayout_search_content, EmptyViewType.SEARCH)
    }

    override fun onSearchButtonClicked(): Observable<String> {
        return searchwidgetview.onSearchClicks().doOnEach { hideKeyboard() }
    }

    override fun getSearchQuery(): String {
        return searchwidgetview.getQuery()
    }

    override fun onStarted() {
        showEmptyView()
        searchwidgetview.onBackButtonClicked {
            hideKeyboard()
            goBack()
        }
    }

    override fun createPresenter(): SearchPresenter {
        return SearchPresenter(this)
    }

    override fun layoutId(): Int {
        return R.layout.fragment_search
    }

    override fun setArtistsList(artists: List<MusicItemData>) {
        val recyclerView = createRecyclerView()

        if (recyclerView != null) {
            replaceFrameLayoutView(framelayout_search_content, recyclerView)
            recyclerView.adapter = object : MusicItemDataAdapter(artists, { artist -> showArtistAlbums(artist) }) {}
        }
    }

    override fun showArtistAlbums(artist: MusicItemData) {
        showFragment(
            Feature.TOP_ALBUMS,
            TopAlbumsFragment.bundle(artist.name, "")
        )
    }
}