package com.abdrew.fmlast.ui.albumdetails

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.abdrew.fmlast.R
import com.abdrew.fmlast.api.model.albuminfo.TrackJson
import com.abdrew.fmlast.base.fragment.BaseFragment
import com.abdrew.fmlast.extensions.initBackButton
import com.abdrew.fmlast.extensions.initToolbar
import com.abdrew.fmlast.view.AlbumDetailsHeaderView
import com.abdrew.fmlast.view.AlbumWikiView
import com.abdrew.fmlast.view.tracklist.TracklistView
import kotlinx.android.synthetic.main.fragment_album_details.*
import kotlinx.android.synthetic.main.toolbar.*

private const val ARTIST_NAME_KEY = "artist_name_key"
private const val ALBUM_NAME_KEY = "album_name_key"

class AlbumDetailsFragment : BaseFragment<AlbumDetailsView, AlbumDetailsPresenter>(), AlbumDetailsView {
    override fun setTrackList(trackJsons: List<TrackJson>) {
        context?.let {
            val tracklist = TracklistView(it)
            tracklist.setTrackList(trackJsons)
            addContent(tracklist)
        }
    }

    override fun setWiki(preview: String, full: String) {
        context?.let {
            val albumWikiView = AlbumWikiView(it)
            albumWikiView.setWiki(preview, full)
            addContent(albumWikiView)
        }
    }

    override fun setAlbumData(artistName: String, albumName: String, listenersCount: String, albumCoverUrl: String) {
        context?.let {
            val headerView = AlbumDetailsHeaderView(it)
            addContent(headerView)
            headerView.setAlbumData(artistName, albumName, listenersCount, albumCoverUrl)
        }
    }

    private fun addContent(view: View) = linearlayout_content.addView(view)

    override fun clearContentView() = linearlayout_content.removeAllViews()

    private var albumName: String? = null
    private var artistName: String? = null

    override fun getArtistName(): String? = artistName

    override fun getAlbumName(): String? = albumName

    override fun onStarted() {
        arguments?.let {
            albumName = it.getString(ALBUM_NAME_KEY)
            artistName = it.getString(ARTIST_NAME_KEY)
        }

        initToolbar(application_toolbar) {
            it.setTitle(R.string.details_toolbar_title)
            it.initBackButton(this)
        }
    }

    override fun createPresenter(): AlbumDetailsPresenter {
        return AlbumDetailsPresenter(this)
    }

    override fun layoutId(): Int {
        return R.layout.fragment_album_details
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                goBack()
                true
            }
            else -> false
        }
    }

    companion object {
        fun bundle(artistName: String?, albumName: String?): Bundle {
            return Bundle().apply {
                putString(ARTIST_NAME_KEY, artistName)
                putString(ALBUM_NAME_KEY, albumName)
            }
        }
    }

}