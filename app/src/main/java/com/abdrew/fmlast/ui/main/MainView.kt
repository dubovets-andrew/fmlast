package com.abdrew.fmlast.ui.main

import com.abdrew.fmlast.base.fragment.BaseFragmentView
import com.abdrew.fmlast.model.Album
import io.reactivex.Observable


interface MainView : BaseFragmentView {
    fun showEmptyView()
    fun showSavedAlbums(albums: List<Album>)
    fun goToSearch()
    fun goToAlbumDetails(artistName: String?, albumName: String?)

    fun onSearchIconClicked(): Observable<Unit>
}