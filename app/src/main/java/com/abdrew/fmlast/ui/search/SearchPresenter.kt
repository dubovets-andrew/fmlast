package com.abdrew.fmlast.ui.search

import com.abdrew.fmlast.base.fragment.BasePresenter
import com.abdrew.fmlast.repositories.MusicRepository
import io.reactivex.disposables.Disposable
import org.koin.java.standalone.KoinJavaComponent.inject


class SearchPresenter(fragmentView: SearchView) : BasePresenter<SearchView>(fragmentView) {
    private val repository: MusicRepository by inject(MusicRepository::class.java)

    override fun addViewDisposables() {
        addViewDisposable(searchArtist())
    }

    private fun searchArtist(): Disposable {
        val mainScheduler = repository.mainScheduler

        return viewInterface.onSearchButtonClicked()
            .doOnEach { viewInterface.showLoader(true) }
            .observeOn(mainScheduler)
            .map { viewInterface.getSearchQuery() }
            .observeOn(repository.ioScheduler)
            .flatMapMaybe { repository.searchArtist(it) }
            .observeOn(mainScheduler)
            .doOnEach { viewInterface.showLoader(false) }
            .subscribe(
                { it -> viewInterface.setArtistsList(it) },
                { t: Throwable? -> t.toString() })
    }

}