package com.abdrew.fmlast.ui.topalbums

import com.abdrew.fmlast.base.fragment.BaseFragmentView
import com.abdrew.fmlast.model.MusicItemData


interface TopAlbumsView : BaseFragmentView {
    fun showTopAlbums(albums: List<MusicItemData>)
    fun getArtistName(): String
    fun goToAlbumDetails(artistName: String?, albumName: String?)
}