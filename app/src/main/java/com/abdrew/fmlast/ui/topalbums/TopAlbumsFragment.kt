package com.abdrew.fmlast.ui.topalbums

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.abdrew.fmlast.Feature
import com.abdrew.fmlast.R
import com.abdrew.fmlast.base.fragment.BaseFragment
import com.abdrew.fmlast.extensions.createRecyclerView
import com.abdrew.fmlast.extensions.initBackButton
import com.abdrew.fmlast.extensions.initToolbar
import com.abdrew.fmlast.extensions.replaceFrameLayoutView
import com.abdrew.fmlast.model.MusicItemData
import com.abdrew.fmlast.ui.albumdetails.AlbumDetailsFragment
import com.abdrew.fmlast.ui.list.MusicItemDataAdapter
import com.abdrew.fmlast.view.checkload.CheckLoadButton
import kotlinx.android.synthetic.main.fragment_top_albums.*
import kotlinx.android.synthetic.main.toolbar.*

private const val ARTIST_NAME_KEY: String = "artist_name_key"
private const val ARTIST_MBID_KEY: String = "artist_mbid_key"

class TopAlbumsFragment : BaseFragment<TopAlbumsView, TopAlbumsPresenter>(), TopAlbumsView {
    override fun goToAlbumDetails(artistName: String?, albumName: String?) {
        showFragment(Feature.ALBUM_DETAILS, AlbumDetailsFragment.bundle(artistName, albumName))
    }

    private var artistName: String? = null

    override fun showTopAlbums(albums: List<MusicItemData>) {
        val recyclerView = createRecyclerView()

        if (recyclerView != null) {
            replaceFrameLayoutView(framelayout_top_albums, recyclerView)

            recyclerView.adapter = object :
                MusicItemDataAdapter(albums, { topAlbum -> goToAlbumDetails(artistName, topAlbum.name) }) {
                override fun actionView(item: MusicItemData): View? {
                    return context?.let {
                        CheckLoadButton(it).apply { initButton(item.name, this@TopAlbumsFragment.artistName) }
                    }
                }
            }
        }
    }

    override fun getArtistName(): String {
        return artistName ?: ""
    }

    override fun onStarted() {
        setHasOptionsMenu(false)

        artistName = arguments?.getString(ARTIST_NAME_KEY)

        initToolbar(application_toolbar) { actionBar ->
            actionBar.initBackButton(this)
            actionBar.title = arguments?.getString(ARTIST_NAME_KEY)
        }

        onBackListener = {
            goBack()
            true
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                goBack()
                true
            }
            else -> false
        }
    }

    override fun createPresenter(): TopAlbumsPresenter {
        return TopAlbumsPresenter(this)
    }

    override fun layoutId(): Int {
        return R.layout.fragment_top_albums
    }

    companion object {
        fun bundle(name: String, mbid: String?): Bundle {
            return Bundle().apply {
                putString(ARTIST_NAME_KEY, name)
                putString(ARTIST_MBID_KEY, mbid)
            }
        }
    }

}