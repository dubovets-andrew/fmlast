package com.abdrew.fmlast.ui.list

import android.view.View
import com.abdrew.fmlast.R
import com.abdrew.fmlast.base.list.BaseAdapter
import com.abdrew.fmlast.model.MusicItemData


abstract class MusicItemDataAdapter(
    data: List<MusicItemData>,
    onItemClickListener: (MusicItemData) -> Unit
) :
    BaseAdapter<MusicItemViewHolder, MusicItemData>(data, onItemClickListener) {
    override fun createHolder(view: View): MusicItemViewHolder {
        return MusicItemViewHolder(view)
    }

    override fun layoutId(): Int = R.layout.view_music_item

    override fun onBindViewHolder(holder: MusicItemViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)

        val musicItemData = data[position]

        actionView(musicItemData)?.let { holder.addActionView(it) }
    }

    open fun actionView(item: MusicItemData): View? = null
}
