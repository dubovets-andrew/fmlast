package com.abdrew.fmlast.ui.main

import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import com.abdrew.fmlast.Feature
import com.abdrew.fmlast.R
import com.abdrew.fmlast.base.fragment.BaseFragment
import com.abdrew.fmlast.base.list.BaseConvertableAdapter
import com.abdrew.fmlast.extensions.createRecyclerView
import com.abdrew.fmlast.extensions.initToolbar
import com.abdrew.fmlast.extensions.replace
import com.abdrew.fmlast.extensions.replaceFrameLayoutView
import com.abdrew.fmlast.model.Album
import com.abdrew.fmlast.model.MusicItemData
import com.abdrew.fmlast.ui.albumdetails.AlbumDetailsFragment
import com.abdrew.fmlast.ui.list.MusicItemViewHolder
import com.abdrew.fmlast.view.empty.EmptyView
import com.abdrew.fmlast.view.empty.EmptyViewType
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.toolbar.*


class MainFragment : BaseFragment<MainView, MainPresenter>(), MainView {
    private val searchClick: PublishRelay<Unit> = PublishRelay.create()

    override fun showEmptyView() {
        context?.let {
            val emptyView = EmptyView(it).apply { setEmptyViewType(EmptyViewType.MAIN) }
            framelayout_main_fragment_content.replace(emptyView)
        }
    }

    override fun onSearchIconClicked(): Observable<Unit> = searchClick

    override fun showSavedAlbums(albums: List<Album>) {
        val recyclerView = createRecyclerView()

        if (recyclerView != null) {
            replaceFrameLayoutView(framelayout_main_fragment_content, recyclerView)
            recyclerView.adapter = object :
                BaseConvertableAdapter<Album, MusicItemData, MusicItemViewHolder>(
                    albums,
                    { album -> MusicItemData.fromAlbum(album) },
                    { goToAlbumDetails(it.artistName, it.albumName) }) {
                override fun createHolder(view: View): MusicItemViewHolder = MusicItemViewHolder(view)

                override fun layoutId(): Int = R.layout.view_music_item
            }
        }

    }

    override fun goToAlbumDetails(artistName: String?, albumName: String?) {
        showFragment(Feature.ALBUM_DETAILS, AlbumDetailsFragment.bundle(artistName, albumName))
    }

    override fun goToSearch() {
        showFragment(Feature.ARTIST_SEARCH)
    }

    override fun createPresenter(): MainPresenter = MainPresenter(this)

    override fun layoutId(): Int = R.layout.fragment_main

    override fun onStarted() {
        showEmptyView()

        initToolbar(application_toolbar) {
            setHasOptionsMenu(true)
            it.setTitle(R.string.main_toolbar_title)
        }

        onBackListener = {
            true
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.main, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.menu_main_search -> {
                searchClick.accept(Unit)
                true
            }
            else -> false
        }
    }
}
