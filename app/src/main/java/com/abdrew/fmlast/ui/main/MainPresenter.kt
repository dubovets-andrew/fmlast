package com.abdrew.fmlast.ui.main

import android.util.Log
import com.abdrew.fmlast.base.fragment.BasePresenter
import com.abdrew.fmlast.repositories.MusicRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import org.koin.java.standalone.KoinJavaComponent.inject


class MainPresenter(fragmentView: MainView) : BasePresenter<MainView>(fragmentView) {
    private val musicRepository: MusicRepository by inject(MusicRepository::class.java)

    override fun addViewDisposables() {
        addViewDisposable(onSearchIconClicked())
        getSavedAlbums()
    }

    private fun onSearchIconClicked(): Disposable {
        return viewInterface.onSearchIconClicked()
            .subscribeOn(AndroidSchedulers.mainThread())
            .subscribe({ it ->
                viewInterface.goToSearch()
            }, {
                Log.d("Error: ", it.message)
            })
    }

    private fun getSavedAlbums(): Disposable {
        return musicRepository.getSavedAlbums()
            .doOnSubscribe { viewInterface.showLoader(true) }
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally { viewInterface.showLoader(false) }
            .subscribe({
                viewInterface.apply {
                    if (it.isNotEmpty()) viewInterface.showSavedAlbums(it)
                    else viewInterface.showEmptyView()
                }
            }, { it ->
                Log.d("Error: ", it.message)
            })
    }
}