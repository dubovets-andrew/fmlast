package com.abdrew.fmlast.ui.search

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import com.abdrew.fmlast.R
import com.abdrew.fmlast.extensions.inflate
import com.jakewharton.rxrelay2.BehaviorRelay
import io.reactivex.BackpressureStrategy
import io.reactivex.Observable
import kotlinx.android.synthetic.main.view_search.view.*


class SearchWidgetView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {
    private val onSearchClicks: BehaviorRelay<String> = BehaviorRelay.create()

    init {
        orientation = HORIZONTAL
        inflate(R.layout.view_search)

        textview_search.setOnClickListener { _ ->
            val query = getQuery()
            onSearchClicks.accept(query)
        }
    }

    public fun onSearchClicks(): Observable<String> {
        return onSearchClicks.toFlowable(BackpressureStrategy.LATEST)
            .toObservable()
            .hide()
    }

    public fun onBackButtonClicked(listener: () -> Unit) {
        imagebutton_back.setOnClickListener { _ -> listener.invoke() }
    }

    public fun getQuery() = edittext_search_query.text.toString()
}