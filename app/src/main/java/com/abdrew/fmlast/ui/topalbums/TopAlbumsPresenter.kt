package com.abdrew.fmlast.ui.topalbums

import com.abdrew.fmlast.base.fragment.BasePresenter
import com.abdrew.fmlast.repositories.MusicRepository
import io.reactivex.disposables.Disposable
import org.koin.java.standalone.KoinJavaComponent.inject


class TopAlbumsPresenter(fragmentView: TopAlbumsView) : BasePresenter<TopAlbumsView>(fragmentView) {
    private val musicRepository: MusicRepository by inject(MusicRepository::class.java)

    override fun addViewDisposables() {
        addViewDisposable(searchTopAlbums())
    }

    private fun searchTopAlbums(): Disposable {
        return musicRepository.getTopAlbums(viewInterface.getArtistName())
            .doOnSubscribe { viewInterface.showLoader(true) }
            .observeOn(musicRepository.mainScheduler)
            .doOnSuccess { viewInterface.showLoader(false) }
            .subscribe({ it -> viewInterface.showTopAlbums(it) }, { t: Throwable? -> t.toString() })
    }
}