package com.abdrew.fmlast.converter

import com.abdrew.fmlast.api.model.albuminfo.AlbumInfoJson
import com.abdrew.fmlast.api.model.albuminfo.TrackJson
import com.abdrew.fmlast.database.model.AlbumEntity
import com.abdrew.fmlast.database.model.TrackEntity
import com.abdrew.fmlast.model.Album
import com.abdrew.fmlast.model.Track


fun AlbumInfoJson.toData(): Album {
    val tracks: List<Track>? = tracks?.trackJsons?.map { it.toData() }
    return Album(null, artist, name, listeners, wiki?.content, wiki?.summary, images[1].path, images[2].path, tracks)
}

fun TrackJson.toData(): Track {
    return Track(null, null, name, duration)
}

fun TrackJson.toEntity(albumId: Long?): TrackEntity {
    return TrackEntity(null, albumId, name, duration)
}

fun Album.toEntity(): AlbumEntity {
    return AlbumEntity(uid, artistName, albumName, listeners, wikiPreview, wikiFull, imageCoverSmall, imageCoverLarge)
}

fun AlbumEntity.toData(): Album {
    return Album(uid, artistName, albumName, listeners, wikiPreview, wikiFull, imageCoverSmall, imageCoverLarge, null)
}

fun Track.toEntity(): TrackEntity {
    return TrackEntity(uid, albumId, trackName, trackDuration)
}