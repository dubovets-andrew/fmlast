package com.abdrew.fmlast.api.model

import com.google.gson.annotations.SerializedName

data class Image(
    @SerializedName("#text") val path: String,
    @SerializedName("size") val size: String
)