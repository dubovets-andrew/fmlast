package com.abdrew.fmlast.api.model.artists

import com.google.gson.annotations.SerializedName


data class FoundArtists(
    @SerializedName("artist") val artists: List<Artist>
)