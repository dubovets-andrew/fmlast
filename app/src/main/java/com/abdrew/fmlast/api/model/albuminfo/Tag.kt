package com.abdrew.fmlast.api.model.albuminfo

import com.google.gson.annotations.SerializedName

data class Tag(
    @SerializedName("name") val name: String
)