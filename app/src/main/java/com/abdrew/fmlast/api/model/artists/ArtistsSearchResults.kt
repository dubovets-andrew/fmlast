package com.abdrew.fmlast.api.model.artists

import com.google.gson.annotations.SerializedName

data class ArtistsSearchResults(
    @SerializedName("artistmatches") val artistMatches: FoundArtists
)