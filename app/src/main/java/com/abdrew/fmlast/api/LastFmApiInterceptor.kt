package com.abdrew.fmlast.api

import com.abdrew.fmlast.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response

class LastFmApiInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val url = originalRequest.url()

        val newUrl = url.newBuilder()
            .addQueryParameter(API_KEY_KEY, BuildConfig.LAST_FM_API_KEY)
            .addQueryParameter(FORMAT_KEY, FORMAT_VALUE)
            .build()

        val newRequest = originalRequest.newBuilder()
            .url(newUrl)
            .build()

        return chain.proceed(newRequest)
    }
}