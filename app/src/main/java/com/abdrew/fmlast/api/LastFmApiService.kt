package com.abdrew.fmlast.api

import com.abdrew.fmlast.api.model.SearchReposnse
import com.abdrew.fmlast.api.model.albuminfo.AlbumInfoResponse
import com.abdrew.fmlast.api.model.artists.ArtistsSearchResults
import com.abdrew.fmlast.api.model.topalbums.TopAlbumsSearchReponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

const val API_KEY_KEY = "api_key"

const val FORMAT_KEY = "format"
const val FORMAT_VALUE = "json"

interface LastFmApiService {
    @GET("?method=artist.search")
    fun searchArtist(@Query("artist") artist: String): Single<SearchReposnse<ArtistsSearchResults>>

    @GET("?method=artist.gettopalbums")
    fun searchTopAlbums(@Query("artist") artist: String): Single<TopAlbumsSearchReponse>

    @GET("?method=album.getinfo")
    fun searchAlbumInfo(
        @Query("artist") artist: String,
        @Query("album") albumName: String
    ): Single<AlbumInfoResponse>
}