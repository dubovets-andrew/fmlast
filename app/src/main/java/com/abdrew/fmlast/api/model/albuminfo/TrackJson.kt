package com.abdrew.fmlast.api.model.albuminfo

import com.google.gson.annotations.SerializedName

data class TrackJson(
    @SerializedName("name") val name: String,
    @SerializedName("duration") val duration: String
)