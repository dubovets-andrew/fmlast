package com.abdrew.fmlast.api.model.topalbums

import com.abdrew.fmlast.api.model.Image
import com.google.gson.annotations.SerializedName

data class TopAlbum(
    @SerializedName("name") val name: String,
    @SerializedName("playcount") val playcount: String,
    @SerializedName("url") val url: String,
    @SerializedName("image") val covers: List<Image>
)