package com.abdrew.fmlast.api.model.albuminfo

import com.google.gson.annotations.SerializedName

data class Tracks(
    @SerializedName("track") val trackJsons: List<TrackJson>
)