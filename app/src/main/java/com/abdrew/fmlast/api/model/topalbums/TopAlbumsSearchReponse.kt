package com.abdrew.fmlast.api.model.topalbums

import com.google.gson.annotations.SerializedName

data class TopAlbumsSearchReponse(
    @SerializedName("topalbums") val topAlbums: TopAlbums
)