package com.abdrew.fmlast.api.model.albuminfo

import com.google.gson.annotations.SerializedName

data class Wiki(
    @SerializedName("content") val content: String,
    @SerializedName("summary") val summary: String,
    @SerializedName("published") val published: String
)