package com.abdrew.fmlast.api.model.artists

import com.abdrew.fmlast.api.model.Image
import com.google.gson.annotations.SerializedName

data class Artist(
    @SerializedName("name") val name: String,
    @SerializedName("image") val covers: List<Image>,
    @SerializedName("url") val url: String,
    @SerializedName("nmbid") val mbid: String,
    @SerializedName("listeners") val listeners: String
)