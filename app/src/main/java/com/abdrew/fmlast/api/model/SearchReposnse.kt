package com.abdrew.fmlast.api.model

import com.google.gson.annotations.SerializedName

data class SearchReposnse<SearchResult : Any>(
    @SerializedName("results") val results: SearchResult
)