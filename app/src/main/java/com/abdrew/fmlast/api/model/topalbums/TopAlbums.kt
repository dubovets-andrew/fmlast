package com.abdrew.fmlast.api.model.topalbums

import com.google.gson.annotations.SerializedName

data class TopAlbums(
    @SerializedName("album") val albums: List<TopAlbum>
)