package com.abdrew.fmlast.api.model.albuminfo

import com.google.gson.annotations.SerializedName

data class AlbumInfoResponse(
    @SerializedName("album") val album: AlbumInfoJson
)