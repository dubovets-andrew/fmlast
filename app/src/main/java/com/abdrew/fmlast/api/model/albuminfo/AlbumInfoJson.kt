package com.abdrew.fmlast.api.model.albuminfo

import com.abdrew.fmlast.api.model.Image
import com.google.gson.annotations.SerializedName

data class AlbumInfoJson(
    @SerializedName("name") val name: String,
    @SerializedName("artist") val artist: String,
    @SerializedName("image") val images: List<Image>,
    @SerializedName("listeners") val listeners: String,
    @SerializedName("playcount") val playCount: String,
    @SerializedName("tracks") val tracks: Tracks?,
    @SerializedName("wiki") val wiki: Wiki?
)