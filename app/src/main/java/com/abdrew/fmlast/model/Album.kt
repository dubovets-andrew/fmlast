package com.abdrew.fmlast.model

data class Album(
    val uid: Int?,
    val artistName: String?,
    val albumName: String?,
    val listeners: String?,
    val wikiPreview: String?,
    val wikiFull: String?,
    val imageCoverSmall: String?,
    val imageCoverLarge: String?,
    val tracks: List<Track>?
)