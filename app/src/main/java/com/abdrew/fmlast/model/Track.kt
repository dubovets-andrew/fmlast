package com.abdrew.fmlast.model

data class Track(
    val uid: Long?,
    val albumId: Long?,
    val trackName: String?,
    val trackDuration: String?
)