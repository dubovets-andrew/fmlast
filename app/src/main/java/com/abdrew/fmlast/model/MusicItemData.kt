package com.abdrew.fmlast.model

import com.abdrew.fmlast.api.model.artists.Artist
import com.abdrew.fmlast.api.model.topalbums.TopAlbum

data class MusicItemData(
    val name: String,
    val imageUrl: String?,
    val listenersCount: String
) {
    companion object {
        fun fromArtist(artist: Artist): MusicItemData = MusicItemData(
            artist.name,
            artist.covers[2].path,
            artist.listeners
        )

        fun fromTopAlbum(albumInfo: TopAlbum): MusicItemData = MusicItemData(
            albumInfo.name,
            albumInfo.covers[2].path,
            albumInfo.playcount
        )

        fun fromAlbum(albumInfo: Album): MusicItemData = MusicItemData(
            albumInfo.albumName ?: "",
            albumInfo.imageCoverLarge,
            albumInfo.listeners ?: ""
        )
    }
}
