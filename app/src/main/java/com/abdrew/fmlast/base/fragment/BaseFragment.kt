package com.abdrew.fmlast.base.fragment

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.abdrew.fmlast.Feature
import com.abdrew.fmlast.MainActivity
import com.abdrew.fmlast.R
import com.abdrew.fmlast.extensions.setVisibleOrGone
import kotlinx.android.synthetic.main.fragment_base.*
import kotlinx.android.synthetic.main.layout_loader.*


abstract class BaseFragment<FragmentViewInterface, Presenter> : Fragment()
        where FragmentViewInterface : BaseFragmentView,
              Presenter : BasePresenter<FragmentViewInterface> {
    private var presenter: Presenter? = null

    var onBackListener: () -> Boolean = { false }

    final override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflate(R.layout.fragment_base, container)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = createPresenter()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        inflateFragmentLayout()
    }

    private fun inflateFragmentLayout() {
        inflate(layoutId(), framelayout_fragment_content, attachToRoot = true)
    }

    private fun inflate(@LayoutRes layoutId: Int, parent: ViewGroup?, attachToRoot: Boolean = false): View =
        LayoutInflater.from(context).inflate(layoutId, parent, attachToRoot)

    override fun onStart() {
        super.onStart()
        onStarted()
        initBackListener()
    }

    override fun onResume() {
        super.onResume()
        presenter?.addViewDisposables()
    }

    private fun initBackListener() {
        view?.let {
            it.isFocusableInTouchMode = true
            it.requestFocus()
            it.setOnKeyListener { _, _, keyEvent ->
                val keyCode = keyEvent?.keyCode
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    onBackListener.invoke()
                } else false
            }
        }
    }

    override fun onPause() {
        super.onPause()
        presenter?.dispose()
    }

    override fun onDestroy() {
        presenter?.dispose()
        presenter = null
        super.onDestroy()
    }

    protected fun showFragment(feature: Feature, bundle: Bundle = Bundle()) {
        (activity as MainActivity).goTo(feature, bundle)
    }

    protected fun goBack() {
        (activity as MainActivity).goBack()
    }

    abstract fun onStarted()

    abstract fun createPresenter(): Presenter

    @LayoutRes
    abstract fun layoutId(): Int

    fun showLoader(isLoading: Boolean) = linearlayout_loader.setVisibleOrGone(isLoading)
}