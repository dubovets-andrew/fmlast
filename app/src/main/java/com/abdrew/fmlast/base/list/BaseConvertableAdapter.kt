package com.abdrew.fmlast.base.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView

open abstract class BaseConvertableAdapter<StoredItem, HolderItem, VHolder>(
    protected val data: List<StoredItem>,
    private val converter: (StoredItem) -> HolderItem,
    private val onItemClickListener: (StoredItem) -> Unit = { }
) : RecyclerView.Adapter<VHolder>()
        where StoredItem : Any,
              HolderItem : Any, VHolder : BaseViewHolder<HolderItem> {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(layoutId(), parent, false)

        return createHolder(view)
    }

    abstract fun createHolder(view: View): VHolder

    @LayoutRes
    abstract fun layoutId(): Int

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: VHolder, position: Int) {
        val chosenItem = data[position]

        holder.bind(converter.invoke(chosenItem))
        holder.setOnClickListener { onItemClickListener.invoke(chosenItem) }
    }
}