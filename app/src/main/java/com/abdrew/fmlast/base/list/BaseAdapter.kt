package com.abdrew.fmlast.base.list


abstract class BaseAdapter<VHolder, Item>(data: List<Item>, onItemClickListener: (Item) -> Unit = { }) :
    BaseConvertableAdapter<Item, Item, VHolder>(
        data,
        { item -> item },
        onItemClickListener
    ) where Item : Any, VHolder : BaseViewHolder<Item>