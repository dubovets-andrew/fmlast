package com.abdrew.fmlast.base.fragment


interface BaseFragmentView : BaseView {
    fun showLoader(isLoading: Boolean)
}