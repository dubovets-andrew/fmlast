package com.abdrew.fmlast.base.list

import android.view.View
import androidx.recyclerview.widget.RecyclerView


abstract class BaseViewHolder<Item>(itemView: View) : RecyclerView.ViewHolder(itemView)
        where Item : Any {
    abstract fun bind(item: Item)

    public open fun setOnClickListener(listener: () -> Unit) {
        itemView.setOnClickListener { listener.invoke() }
    }
}
