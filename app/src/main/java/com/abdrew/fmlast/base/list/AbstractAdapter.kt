package com.abdrew.fmlast.base.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView

abstract class AbstractAdapter<VHolder, Item>(val data: List<Item>) : RecyclerView.Adapter<VHolder>()
        where Item : Any,
              VHolder : BaseViewHolder<Item> {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(layoutId(), parent, false)

        return createHolder(view)
    }

    abstract fun createHolder(view: View): VHolder

    @LayoutRes
    abstract fun layoutId(): Int

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: VHolder, position: Int) = holder.bind(data[position])
}