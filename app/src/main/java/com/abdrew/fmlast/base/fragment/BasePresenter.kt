package com.abdrew.fmlast.base.fragment

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BasePresenter<BaseViewInterface : BaseView>(protected var viewInterface: BaseViewInterface) {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    fun addViewDisposable(disposable: Disposable) = compositeDisposable.add(disposable)

    open fun dispose() = compositeDisposable.clear()

    abstract fun addViewDisposables()
}