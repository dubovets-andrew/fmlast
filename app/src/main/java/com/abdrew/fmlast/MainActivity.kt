package com.abdrew.fmlast

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    private val fragmentCreator: FragmentCreator = FragmentCreator(supportFragmentManager)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        goTo(Feature.MAIN, Bundle())
    }

    public fun goTo(feature: Feature, bundle: Bundle) {
        fragmentCreator.goTo(feature, bundle)
    }

    public fun goBack() {
        fragmentCreator.goBack()
    }
}
